require 'mongoid'
require 'json'
require_relative '../lib/hash_decoder'

Mongoid.load!(File.join(File.dirname(__FILE__), '..', 'config', 'mongoid.yml'))

class Url
  include Mongoid::Document

  field :long_url, type: String
  field :short_url, type: String

  def self.get_long_url(short_url)
    self.where(short_url: short_url).pluck(:long_url).first
  end

  def self.get_short_url(long_url)
    db_url = get_short_url_from_db(long_url)
    return db_url unless db_url.nil?

    create_row_and_get_short_url(long_url)
  end

  private

  def self.create_row_and_get_short_url(long_url)
    url = self.new
    url.long_url = long_url
    url.short_url = create_short_url(url._id)

    result = url.save!
    Throw new Exception('Cant save to DB') unless result
    url.short_url
  end

  def self.create_short_url(id)
    base62 = [ *0..9, *'a'..'z', *'A'..'Z'].join

    HashDecoder.encode( id, base62)
  end

  def self.get_short_url_from_db(long_url)
    self.where(long_url: long_url).pluck(:short_url).first
  end

end