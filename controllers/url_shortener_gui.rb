require 'sinatra'
require_relative '../models/url'

set :root, File.join(File.dirname(__FILE__), '..')
set :views, Proc.new { File.join(root, 'views') }

# Gui for fun and tests
# before it you should run mongodb locally, or remote but then you should edit 'config/mongoid.yml'
# run it right here by Run/run

# not in fiber, not in em. just sinatra and mongodb because this is outscope from test task.

before do
  cache_control :public
end

get '/' do
  status 200

  erb :form
end

# you should post full address like 'http://www.rubydoc.info/'
post '/' do
  status 200

  @short_url = Url.get_short_url(params[:message])

  erb :form
end

get '/:short_url' do
  redirect Url.get_long_url(params['short_url'])
end

not_found do
  halt 404, 'page not found'
end