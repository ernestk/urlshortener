require "sinatra"
require_relative "../models/url"

class UrlShortener < Sinatra::Base

  configure do
    set :threaded, false
  end

  before do
    cache_control :public
  end

  get '/hello' do
    'Hello World!'
  end

  post '/' do
    Fiber.new {
      data = JSON.parse request.body.read
      response = Rack::Response.new(
                                   body = [{ :url => "http://127.0.0.1/#{Url.get_short_url(data['longUrl'])}"}.to_json],
                                   status = 200,
                                   header = {'Content-Type' => 'application/json'}
      )

      response.finish
    }.resume
  end

  get '/:short_url' do
    Fiber.new {
      response = Rack::Response.new(
          body = ["location: #{Url.get_long_url(params['short_url'])}"],
          status = 301
      )

      response.finish
    }.resume
  end

  not_found do
    halt 404, 'page not found'
  end

end
