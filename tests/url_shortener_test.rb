require_relative '../controllers/url_shortener'
require 'minitest/autorun'
require 'rack/test'
require_relative '../models/url'


class UrlShortenerTest < Minitest::Test
  include Rack::Test::Methods

  def app
    UrlShortener
  end

  def test_my_default
    get '/hello'

    assert last_response.ok?
    assert_equal 'Hello World!', last_response.body
  end

  def test_post_link
    input_data = '{
        "longUrl": "https://www.rust-lang.org/ru-RU/documentation.html"
    }'
    expected_data = '{
        "url": "http://127.0.0.1/4CScWXh1jGHKObn3dkpJI"
    }'

    post '/', input_data , {'Content-Type' => 'application/json'}

    assert last_response.ok?
    assert_equal JSON.parse(expected_data), JSON.parse(last_response.body)
  end

  def test_redirect_response
    long_url = 'https://www.rust-lang.org/ru-RU/documentation.html'
    short_url = Url.get_short_url(long_url)

    get "/#{short_url}"

    assert last_response.redirection?
    assert_equal "location: #{long_url}", last_response.body
  end

end