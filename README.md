# README #

База данных mongodb, (так как она у вас используется, да и попробовать ее)                 
поставил ее локально.              
Для того чтобы запустить приложение необходимо ее поставить и запустить.

Далее необходимо в консоли выполнить 
```
#!ruby

ruby bin/start_url_shortener
```

Приложение запустится.                  
Приложение делает то, что требуется в задании "https://medium.com/@Makdixi/b90101843b4b#.j4l51165y"
-----------------------

Также есть файл "controllers/url_shortener_gui.rb".             
Он не использует EM, fiber( в задании не было gui, так что это бонус, а не часть задания)           
Сделал его чтобы тестить и глазами смотреть на данные.

Запустить его можно руками прямо из файла.              
Для него тоже нужна запущенная БД.            
Показывает в простенькой форме ссылки.            
Редиректит на страницу по короткой ссылке.
-----------------------

Хэш получился длинноват, из-за того что основан он на object_id поле mongi.              
Зато он уникален, так как поле object_id уникальное.             

Вводить автокинкремент и рассчитывать хэш на его основе               
не хотел по разным причинам:              

* гемы которые существуют не показались мне надежными.
* говорят что если потом шардировать бд, то будут проблемы.

Плохая вообщем практика, решил не привыкать.              
Обрезать ссылки на основе гема bit.ly, мне совсем не нравится идея.            
-----------------------

Потратил не 2 дня( меньше).            
Немного отвлекался на собеседования, так что это не было 2 дня "запойного" кодинга.           

ps если что-то не запускается пишите, приложение рабочее и я им горд:)