module HashDecoder
  ENCODER = Hash.new do |h,k|
    h[k] = Hash[ k.chars.map.with_index.to_a.map(&:reverse) ]
  end
  def self.encode( value, keys )
    ring = ENCODER[keys]
    base = keys.length
    result = []
    formated_val = value.to_s.to_i(36)
    until formated_val == 0
      result << ring[ formated_val % base ]
      formated_val /= base
    end
    result.reverse.join
  end
end